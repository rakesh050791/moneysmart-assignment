FactoryBot.define do
  factory :link do |f|
    f.in_url { Faker::Internet.url }
  end

  factory :invalid_link, parent: :link do |f|
    f.in_url {'abcd'}
  end
end