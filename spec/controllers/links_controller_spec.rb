require 'rails_helper'

RSpec.describe LinksController, type: :controller do

  before(:all) do
    Link.destroy_all
  end

  let!(:link) {FactoryBot.create(:link)}

  describe 'create' do
    context 'when valid input' do
      before { post :create,:format => 'js', params: {link: FactoryBot.attributes_for(:link)} }
      it 'should increment links count' do
        expect(Link.count).to eq(2)
        expect(TrackUsage.count).to eq(0)
      end

      it 'should redirect create_partial' do
        expect(response).to render_template("links/create")
      end
    end

    context 'when invalid input' do
      before { post :create,:format => 'js', params: {link: FactoryBot.attributes_for(:invalid_link)} }
      it 'should not increment links count' do
        expect(Link.count).to eq(1)
      end

      it 'should give invalid input' do
        expect(flash[:notice]).to match(/^Invalid input/)
      end
    end

    context 'when duplicate input' do
      before  do
        post :create,:format => 'js', params: {link: {in_url: link.in_url}}
      end

      it 'new record shpuld not be created' do
        expect(Link.count).to eq(1)
        expect(response).to render_template("links/create")
      end
    end
  end

  describe 'show' do
    context 'when valid request' do
      before { get :show,:format => 'js', params: {short_code: link.short_code} }
      it 'should redirect to original url' do
        expect(response).to redirect_to link.in_url
      end

      it 'should increment track usages count' do
        expect(link.track_usages.count).to eq(1)
      end
    end

    context 'when invalid request' do
      before { get :show,:format => 'js', params: {short_code: '123abc'} }
      it 'should return invalid url message' do
        expected_response = {"message"=>"Invalid Url.", "status"=>"not_found"}
        expect(JSON.parse(response.body)).to eq(expected_response)
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'new' do
    context 'when new method called' do
      before {get :new, params: {link: {in_url: link.in_url}}}
      it 'should render form' do
        expect(assigns(:link)).to be_a_new(Link)
        expect(response).to render_template(:new)
      end
    end
  end

  describe 'update_clicks' do
    context 'when short url clicked' do
      before {put :update_clicks, params: {id: link.id}}
      it 'should update track usages' do
        expect(link.track_usages.count).to eq(1)
      end
    end
  end

end
