require 'rails_helper'

RSpec.describe Link, type: :model do
  context 'associations' do
    it {is_expected.to have_many(:track_usages)}
  end

  context "validations" do
    it {is_expected.to validate_presence_of :in_url}
    it {is_expected.to validate_uniqueness_of :in_url}

    context "validating url format" do
      it {is_expected.to allow_value('www.crowdint.com').for(:in_url)}
      it {is_expected.to allow_value('http://crowdint.com').for(:in_url)}
      it {is_expected.to allow_value('http://www.crowdint.com').for(:in_url)}

      it {is_expected.to_not allow_value('http://www.crowdint. com').for(:in_url)}
      it {is_expected.to_not allow_value('http://fake').for(:in_url)}
      it {is_expected.to_not allow_value('http:fake').for(:in_url)}
    end
  end

  it "has a valid factory" do
    expect(build(:link)).to be_valid
  end
  it "has a invalid factory" do
    expect(build(:invalid_link)).to be_invalid
  end

  describe "public instance methods" do
    let(:link) {create(:link)}
    context "responds to its methods" do
      it {expect(link).to respond_to(:short_url)}
      it {expect(link).to respond_to(:add_usages)}
    end

    context "executes methods correctly" do
      context "short_url" do
        it "should return short url" do
          expect(link.short_url).to eq("#{BASE_URL}#{link.short_code}")
        end
      end

      context "add_usages" do
        it "should add_or_update track usage" do
          expect(link.add_usages('127.0.0.1')).to eq(true)
        end
      end
    end
  end
end