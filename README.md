# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

*Prerequisites
Assuming you have installed `git`, `ruby` and `rvm`.

* Ruby version
2.4.1

* Rails version
5.1.6

* To setup this application on local
1 - git clone git@bitbucket.org:rakesh050791/moneysmart-assignment.git

2 - go to project directory :  cd url_shortner_app and run : bundle 

3 - rake db:create, db:migrate, db:seed

4 - run server : rails s 

5 - Go to browser and type : localhost:3000 

* How to run the test suite
bundle exec rspec 


* Basic flow
-- user will enter url (long_url) which need's to be shortened

-- this app will process the long url and will check if given url is valid url, then will return the shortened url to the end user

-- when user will click on the shortened url, user will be redirected to the original url.

-- if given url is invalid url, user will see a message saying Invalid url and will clear the form.

-- If given url is already present in our database, then we will not generate new shortened url, will just return the shortened url
already present against the given url.

-- when user will click on shortened url we are also tracking the user's ip address and will increment the clicks count for the url.

-- Admin panel is also built, so that admin's of the application can see the statactics (what shortened url's are created & how many 
clicks on the same. etc.)


*Scope of improvement 
–– We can add expiry for the shortened url or we can add soft delete functionality so that we can control database size

-- User authentication can also be incorporated.

-- Instead of generating simple 6 - 7 digits of short code for shortened url we can use some other better technique's for scaling this solution.


* ...
