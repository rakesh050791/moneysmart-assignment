ActiveAdmin.register Link do
  menu priority: 2
  actions :all, except: [:edit,:new]

  index do
    selectable_column
    id_column
    column :in_url
    # column :clicks
    column "shortened url" do |link|
      link.short_url
    end
    column :created_at
    column :updated_at
    actions
  end
end
