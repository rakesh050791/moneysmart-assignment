ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc {I18n.t("active_admin.dashboard")}

  content title: proc {I18n.t("active_admin.dashboard")} do

    columns do

      column do
        panel "Recent Url's" do
          table_for Link.order('created_at desc').limit(20) do
            column :ShortenedUrl do |list|
              link_to list.short_url, admin_link_path(list)
            end

            column :clicks do |list|
              strong {list.track_usages.sum(:clicks)}
            end

            column :user_ip do |list|
              strong {list.user_ip_address}
            end


            column "Added On" do |list|
              days = ((Time.now.to_date - list.created_at.to_date).to_i).to_s
              if days.eql? ("0")
                status_tag("Today", :ok)
              else
                status_tag(days + " " + "Days Ago", :blue)
              end
            end

          end
        end
      end
    end
  end
end
