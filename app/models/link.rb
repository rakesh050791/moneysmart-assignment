class Link < ApplicationRecord

  include UniqueCodes

  #Validations
  validates :in_url, presence: true
  validates :in_url, uniqueness: true

  validates_format_of :in_url, with: /\A(?:(?:http|https):\/\/)?([-a-zA-Z0-9.]{2,256}\.[a-z]{2,4})\b(?:\/[-a-zA-Z0-9@,!:%_\+.~#?&\/\/=]*)?\z/

  #Associations
  has_many :track_usages, dependent: :destroy

  before_save :generate_short_code

  def short_url
    "#{BASE_URL}#{short_code}"
  end

  def add_usages ip
    usages = track_usages.find_or_initialize_by(user_ip_address: ip)
    usages.clicks += 1
    usages.save
  end

  private

  def generate_short_code
    self.short_code ||= get_small_code(:short_code)
    false
  end

end
