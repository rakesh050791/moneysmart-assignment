module UniqueCodes
  MAX_NUMBER_OF_CHARS = 6

  CHARSET = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a

  # Generates a random, unique and small code
  private

  def get_small_code(field)
    loop do
      code = (0..MAX_NUMBER_OF_CHARS).map { CHARSET[rand(CHARSET.size)] }.join

      return code unless self.class.exists?(field => code)
    end
  end
end