class LinksController < ApplicationController
  before_action :create_link, only: [:create]
  before_action :set_link, only: [:show]
  before_action :get_link, only: [:update_clicks]

  def show
    redirect_to @link.in_url and @link.add_usages(get_ip)
  end

  def new
    @link = Link.new
  end

  def create
    unless @link.save
      render :new
    end
  end

  def update_clicks
    @link.add_usages(get_ip)
  end

  private

  def create_link
    @link = Link.find_or_initialize_by(in_url: link_params[:in_url])
    @link.user_ip_address = get_ip
    when_invalid_url unless @link.valid?
  end

  def when_invalid_url
    flash[:notice] = "Invalid input"
    redirect_to root_path
  end

  def get_ip
    request.remote_ip
  end

  def set_link
    @link = Link.find_by(short_code: params[:short_code])
    render json: {message: "Invalid Url.", status: :not_found} unless @link.present?
  end

  def get_link
    @link = Link.find(params[:id]) if params[:id]
  end

  def link_params
    params.require(:link).permit(:in_url)
  end
end
