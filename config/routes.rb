Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'links#new'
  resources :links
  get '/:short_code', to: "links#show"
  put '/update_clicks', to: "links#update_clicks"
end
