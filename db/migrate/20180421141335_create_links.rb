class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :in_url
      t.string :short_code
      t.string :user_ip_address, default: " "
      t.datetime :expiry

      t.timestamps
    end
    add_index :links, :in_url
  end
end
