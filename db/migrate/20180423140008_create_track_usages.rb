class CreateTrackUsages < ActiveRecord::Migration[5.1]
  def change
    create_table :track_usages do |t|
      t.references :link, foreign_key: true
      t.string :user_ip_address , default: " "
      t.integer :clicks, null: false, default: 0

      t.timestamps
    end
  end
end
